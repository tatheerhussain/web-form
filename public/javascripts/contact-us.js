
$(function () {
   $('.ah-contact-form').on('submit', function (e) {
       e.preventDefault();
       var form = $(e.currentTarget).serialize();
       
       console.log('data = ', form);
       
       $.ajax({
           url: '/contact-us/',
           type: 'POST', 
           data: form,
           success: function (result) {
               console.log('success', result);
           }, 
           error: function (error) {
               console.error('error', error);
           }
       });
       
   });
   
   $('#ah-phone').on('input', function (e) {
       var val = $(e.currentTarget).val();
       var phoneRegex = /^\d*$/;
       
       if(!phoneRegex.test(val)) {
           var newVal = val.substr(0, val.length - 1);
           $(e.currentTarget).val(newVal);
       }
   });
      
   $('#ah-text').on('input', function (e) {
        var nameRegex = /^[A-z ]+$/;
        var name = $(e.currentTarget).val();
        
        if(!nameRegex.test(name)) {
           var newName = name.substr(0, name.length - 1);
           $(e.currentTarget).val(newName);
        }
               
   });
        
});

