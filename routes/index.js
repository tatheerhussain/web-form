var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('home.html', { title: 'Express' });
});


router.get('/contact-us', function (req, res, next) {
    res.render('contact-us.html', {});
});

router.get('/about-us', function (req, res, next) {
    res.render('about-us.html', {});
});


// url for getting contact form data.
router.post('/contact-us', function (req, res, next) {
    console.log(req.body);
    res.send('thank you for contating us.');
})
module.exports = router;
